# covid_19

Proyecto basado en información de
https://api-sports.io/documentation/covid-19

## Didáctico

Uso de librerías tales como:
- [google_fonts](https://pub.dev/packages/google_fonts)
- [http](https://pub.dev/packages/http)
- [intl](https://pub.dev/packages/intl)
- [fl_chart](https://pub.dev/packages/fl_chart)
- [clay_containers](https://pub.dev/packages/clay_containers)
- [flutter_bloc](https://pub.dev/packages/flutter_bloc)
- [equatable](https://pub.dev/packages/equatable)
- [pull_to_refresh](https://pub.dev/packages/pull_to_refresh)

![stats|400x711](/assets/Screenshot_1591197402.png "Información de un determinado país")
![countries|400x711](/assets/Screenshot_1591197243.png "Seleccionar país")

Desarrollado por
[John](mailto://johnmoscoso911@gmail.com).
![insane|204x100](/assets/insane_developer.png "Insane Developer")