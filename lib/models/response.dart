class BaseResponseModel {
  final Map<String, dynamic> payload;

  String get route => payload['get'];
  List<dynamic> get parameters => payload['parameters'];
  List<dynamic> get errors => payload['errors'];
  int get results => payload['results'];
  List<dynamic> get response => payload['response'];

  BaseResponseModel(this.payload);
}
