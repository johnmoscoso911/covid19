class CountryModel {
  final String name;

  CountryModel(this.name);

  @override
  String toString() => name;

  @override
  int get hashCode => name.hashCode;

  @override
  bool operator ==(dynamic o) =>
      identical(this, o) ? true : o is CountryModel && o.name == name;
}
