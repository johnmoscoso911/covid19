import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CasesModel {
  final Map<String, dynamic> payload;

  String get newCases => payload['new'];
  int get active => payload['active'];
  int get critical => payload['critical'];
  int get recovered => payload['recovered'];
  int get total => payload['total'];

  CasesModel(this.payload);
}

class DeathModel {
  final Map<String, dynamic> payload;

  String get newCases => payload['new'];
  int get total => payload['total'];

  DeathModel(this.payload);
}

class TestsModel {
  final Map<String, dynamic> payload;

  int get total => payload['total'] ?? 0;

  TestsModel(this.payload);
}

class StatsModel {
  final Map<String, dynamic> payload;
  var percentFormat =
      NumberFormat.decimalPercentPattern(locale: 'es', decimalDigits: 1);
  var thousandFormat = NumberFormat.decimalPattern('es');

  String get continent => payload['continent'];
  int get population => payload['population'];
  String get country => payload['country'];
  String get day => payload['day'];
  DateTime get time => DateTime.parse(payload['time']);
  CasesModel get cases => CasesModel(payload['cases']);
  DeathModel get deaths => DeathModel(payload['deaths']);
  TestsModel get tests => TestsModel(payload['tests']);

  double get activeFraction => cases.active / cases.total;
  String get totalActive => percentFormat.format(activeFraction);

  double get recoveredFraction => cases.recovered / cases.total;
  String get totalRecovered => percentFormat.format(recoveredFraction);

  double get deathsFraction => deaths.total / cases.total;
  String get totalDeaths => percentFormat.format(deathsFraction);

  double get criticalFraction =>
      cases.active == 0 ? 0 : cases.critical / cases.active;
  String get totalCritical => percentFormat.format(criticalFraction);

  double get testsFraction => tests.total == 0 ? 0 : cases.total / tests.total;
  String get totalTests =>
      tests.total == 0 ? '—' : percentFormat.format(testsFraction);

  double get populationFraction => cases.total / population;
  String get percentOfPopulation => percentFormat.format(populationFraction);

  List<PieChartSectionData> casesSectionData(ThemeData theme) {
    var style = theme.textTheme.subtitle2
        .copyWith(color: Colors.white, fontWeight: FontWeight.w500);
    return [
      PieChartSectionData(
        title: thousandFormat.format(cases.active),
        value: activeFraction,
        radius: 48.0,
        titleStyle: style,
        color: Colors.orange,
      ),
      PieChartSectionData(
        title: thousandFormat.format(cases.recovered),
        value: recoveredFraction,
        radius: 48.0,
        titleStyle: style,
        color: Colors.blue,
      ),
      PieChartSectionData(
        title: thousandFormat.format(deaths.total),
        value: deathsFraction,
        radius: 48.0,
        titleStyle: style,
        color: Colors.red,
      ),
    ];
  }

  // List<PieChartSectionData> criticalSectionData(ThemeData theme) {
  //   var style = theme.textTheme.subtitle2
  //       .copyWith(color: Colors.white, fontWeight: FontWeight.w500);
  //   return [
  //     PieChartSectionData(
  //       title: '${cases.critical}',
  //       value: cases.critical / cases.active,
  //       radius: 48.0,
  //       titleStyle: style,
  //       color: Colors.deepOrange,
  //     ),
  //     PieChartSectionData(
  //       title: '${cases.active - cases.critical}',
  //       value: (cases.active - cases.critical) / cases.active,
  //       radius: 48.0,
  //       titleStyle: style,
  //       color: Colors.orange,
  //     ),
  //   ];
  // }

  StatsModel(this.payload);

  @override
  String toString() =>
      '$country (${thousandFormat.format(cases.total)} / ${thousandFormat.format(population)} -> $percentOfPopulation)';
  // {
  //   country: China,
  // cases: {new: +1, active: 79, critical: 5, recovered: 78280, total: 82993},
  // deaths: {new: null, total: 4634},
  // tests: {total: null},
  // day: 2020-05-27,
  // time: 2020-05-27T16:45:06+00:00
  // }
}
