import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../commons/index.dart';
import '../../models/index.dart';

@immutable
class CountriesState extends Equatable {
  final bool isLoading, hasError;
  final String message;
  final List<CountryModel> countries;

  CountriesState(
      {this.isLoading = false,
      this.hasError = false,
      this.message,
      this.countries});

  factory CountriesState.init() => CountriesState();

  factory CountriesState.loading() => CountriesState(isLoading: true);

  factory CountriesState.error(Exception _) =>
      CountriesState(hasError: true, message: _.toString());

  factory CountriesState.success(List<CountryModel> countries) =>
      CountriesState(countries: countries);

  @override
  List<Object> get props => [isLoading, hasError, message];
}

class CountriesBloc extends Bloc<String, CountriesState> {
  @override
  CountriesState get initialState => CountriesState.init();

  @override
  Stream<CountriesState> mapEventToState(
    String event,
  ) async* {
    yield CountriesState.loading();
    try {
      var response = await RapidAPI.countries(event);
      yield CountriesState.success(response);
    } catch (_) {
      yield CountriesState.error(_);
    }
  }
}
