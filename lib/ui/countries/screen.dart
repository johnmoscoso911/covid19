import 'package:clay_containers/clay_containers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../commons/index.dart';
import '../../models/index.dart';
import '../index.dart';

class CountriesPage extends StatefulWidget {
  final String current;

  CountriesPage({this.current = ''});

  @override
  _CountriesPageState createState() => _CountriesPageState();
}

class _CountriesPageState extends State<CountriesPage> {
  CountriesBloc _bloc;
  List<CountryModel> _countries = [], _filtered = [];
  // var _key = GlobalKey<AnimatedListState>();

  @override
  void initState() {
    super.initState();
    _bloc = CountriesBloc();
    _bloc.add('');
  }

  @override
  void dispose() {
    _bloc?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var top = MediaQuery.of(context).viewPadding.top * 2;

    _waiting() => BlocBuilder<CountriesBloc, CountriesState>(
          builder: (context, state) =>
              state.isLoading ? CovidProgressAnimated() : Center(),
        );

    _list() => Column(
          children: [
            Container(
              height: top,
              // padding: EdgeInsets.symmetric(horizontal: 16.0),
              decoration: BoxDecoration(
                // borderRadius: BorderRadius.vertical(
                //   bottom: Radius.circular(16.0),
                // ),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 4.0),
                    blurRadius: 4.0,
                    color: theme.accentColor,
                  )
                ],
                color: theme.scaffoldBackgroundColor,
                // border: Border(bottom: BorderSide()),
              ),
              child: Stack(
                fit: StackFit.expand,
                children: [
                  Transform.translate(
                    offset: Offset(16.0, 0),
                    child: TextField(
                      autofocus: true,
                      decoration: InputDecoration(border: InputBorder.none),
                      onChanged: (f) => setState(() => _filtered = _countries
                          .where((c) => c.name
                              .contains(RegExp('$f', caseSensitive: false)))
                          .toList()),
                    ),
                  ),
                  Positioned(
                    right: 0,
                    child: Container(
                      width: top,
                      height: top,
                      decoration: BoxDecoration(
                        color: theme.accentColor,
                        // borderRadius: BorderRadius.only(
                        //     bottomRight: Radius.circular(16.0)),
                      ),
                      child: Icon(
                        Icons.search,
                        // color: theme.scaffoldBackgroundColor,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 8.0),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: SingleChildScrollView(
                  //   child: Column(
                  //     children: _countries.map((e) => CountryItem(country: e)).toList(),
                  //   ),
                  // ),
                  child: Wrap(
                    children: _filtered
                        .map((e) => CountryItem(
                              country: e,
                              onTap: () =>
                                  Navigator.of(context).pop<String>(e.name),
                            ))
                        .toList(),
                    spacing: 12.0,
                    runSpacing: 16.0,
                    alignment: WrapAlignment.spaceBetween,
                  ),
                ),
              ),
            ),
          ],
        );
    /*AnimatedList(
        key: _key,
        itemBuilder: (context, index, animation) => null);*/

    return MultiBlocProvider(
      providers: [
        BlocProvider<CountriesBloc>(create: (context) => _bloc),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener<CountriesBloc, CountriesState>(
            listener: (context, state) {
              if (state.hasError)
                debugPrint(state.message);
              else if (!state.isLoading /*&& state.countries != null*/)
                setState(() {
                  _countries = state.countries
                      .where((c) => c.name != widget.current)
                      .toList();
                  _filtered = state.countries
                      .where((c) => c.name != widget.current)
                      .toList();
                });
              // state.countries?.forEach((element) {
              //   _countries.insert(0, element);
              //   _key.currentState
              //       .insertItem(0, duration: Duration(milliseconds: 1000));
              // });
            },
          ),
        ],
        child: SafeArea(
          child: Scaffold(
            appBar: AppBar(
              elevation: 0,
              title: Text('${_filtered.length}'),
            ),
            body: Stack(
              fit: StackFit.expand,
              children: [
                // Background(),
                // Mask(),
                _waiting(),
                _list(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CountryItem extends StatelessWidget {
  final CountryModel country;
  final Function onTap;

  const CountryItem({@required this.country, this.onTap})
      : assert(country != null);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        // boxShadow: [
        //   BoxShadow(
        //     offset: Offset(0.0, 4.0),
        //     color: theme.primaryColor,
        //     blurRadius: 4.0,
        //   )
        // ],
        color: theme.accentColor,
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8.0),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: onTap,
              child: ClayContainer(
                child: Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 16.0, vertical: 8.0),
                  child: ClayText(
                    country.name,
                    color: theme.primaryColor,
                    parentColor: Coolors.metallicSeaweed.shade50,
                    emboss: true,
                    style: theme.textTheme.subtitle1
                        .copyWith(fontWeight: FontWeight.w300),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
