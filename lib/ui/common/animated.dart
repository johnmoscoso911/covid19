import 'package:flutter/material.dart';

class CovidProgressAnimated extends StatefulWidget {
  @override
  _CovidProgressAnimatedState createState() => _CovidProgressAnimatedState();
}

class _CovidProgressAnimatedState extends State<CovidProgressAnimated>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<Color> _light, _dark;

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750))
          ..addListener(() {
            if (_controller.status == AnimationStatus.completed)
              _controller.reverse();
            else if (_controller.status == AnimationStatus.dismissed)
              _controller.forward();
            setState(() {});
          });
    _light = ColorTween(
      begin: Colors.yellow,
      end: Colors.red,
    ).animate(_controller);
    _dark = ColorTween(
      begin: Colors.deepOrange,
      end: Colors.yellowAccent,
    ).animate(_controller);
    _controller.forward();
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: [
        ShaderMask(
          shaderCallback: (rect) =>
              LinearGradient(colors: [_light.value, _dark.value])
                  .createShader(rect),
          child: Center(
            // decoration: BoxDecoration(
            //   image: DecorationImage(
            //     image: AssetImage('assets/covid19.png'),
            //     alignment: Alignment.topRight,
            //     repeat: ImageRepeat.repeatY,
            //   ),
            // ),
            child: Image.asset(
              'assets/covid19.png',
              // fit: BoxFit.contain,
              width: MediaQuery.of(context).size.width * 0.5,
            ),
          ),
        ),
      ],
    );
  }
}
