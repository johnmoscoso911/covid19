import 'package:flutter/material.dart';

class Mask extends StatelessWidget {
  const Mask({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return Container(
      color: theme.scaffoldBackgroundColor.withOpacity(0.95),
    );
  }
}

class Background extends StatelessWidget {
  const Background({
    Key key,
    this.assetName = 'assets/covid19.png',
  }) : super(key: key);

  final String assetName;

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return Container(
      decoration: BoxDecoration(
        // color: Colors.transparent,
        image: DecorationImage(
          image: AssetImage(assetName),
          colorFilter: ColorFilter.mode(theme.primaryColor, BlendMode.srcATop),
          alignment: Alignment.topRight,
          repeat: ImageRepeat.repeatY,
        ),
      ),
    );
  }
}
