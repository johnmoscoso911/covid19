import 'package:flutter/material.dart';

class CovidMenu extends StatelessWidget {
  final Function onTap;

  const CovidMenu({Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return ClipRRect(
      borderRadius: BorderRadius.circular(32.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(32.0),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 4.0),
              color: theme.scaffoldBackgroundColor,
              blurRadius: 4.0,
            )
          ],
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: onTap,
            child: Image.asset('assets/covid19.png'),
          ),
        ),
      ),
    );
  }
}
