import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class Refresher extends SliverPersistentHeaderDelegate {
  final Widget child;
  final Function onRefresh;
  final RefreshController controller;
  final double maxHeight;

  Refresher({
    @required this.child,
    this.onRefresh,
    @required this.controller,
    this.maxHeight = 64.0,
  }) : assert(child != null);

  @override
  Widget build(
          BuildContext context, double shrinkOffset, bool overlapsContent) =>
      SizedBox.expand(
        child: SmartRefresher(
          controller: controller,
          onRefresh: onRefresh,
          child: child,
          enablePullDown: true,
        ),
      );

  @override
  double get maxExtent => maxHeight;

  @override
  double get minExtent => 16.0;

  @override
  bool shouldRebuild(Refresher oldDelegate) => child != oldDelegate.child;
}
// SliverPersistentHeader(
//   pinned: true,
//   delegate: Refresher(
//     maxHeight: height * 0.1,
//     child: Container(
//       decoration: BoxDecoration(
//         border: Border(
//           bottom: BorderSide(color: theme.dividerColor),
//         ),
//       ),
//       child: Padding(
//         padding: const EdgeInsets.symmetric(horizontal: 16.0),
//         child: Stack(
//           fit: StackFit.expand,
//           // mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: [
//             ClayText('H'),
//             Positioned(
//               right: 0,
//               bottom: 8.0,
//               child: ClayContainer(
//                 width: 32.0,
//                 height: 32.0,
//                 borderRadius: 16.0,
//               ),
//             ),
//           ],
//         ),
//       ),
//     ),
//     controller: _controller,
//     onRefresh: () => _bloc.add('Ecuador'),
//   ),
// ),
