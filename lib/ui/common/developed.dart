import 'package:clay_containers/clay_containers.dart';
import 'package:flutter/material.dart';

class Developer extends StatelessWidget {
  const Developer({
    Key key,
    @required this.name,
  }) : super(key: key);

  final String name;

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return ClayContainer(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Image.asset(
              'assets/insane_developer.png',
              height: 32.0,
            ),
            ClayText(
              name,
              style: theme.primaryTextTheme.headline6,
              emboss: true,
              textColor: theme.primaryColor,
            ),
          ],
        ),
      ),
    );
  }
}
