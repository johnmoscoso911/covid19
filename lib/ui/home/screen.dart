import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../index.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  StatsBloc _bloc;
  RefreshController _controller;
  String _current = 'Ecuador';

  @override
  void initState() {
    super.initState();
    _bloc = StatsBloc();
    _bloc.add(_current);
    _controller = RefreshController();
  }

  @override
  void dispose() {
    _controller?.dispose();
    _bloc?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;

    _appBar() => SliverAppBar(
          leading: CovidMenu(onTap: () async {
            var x = await Navigator.of(context).push<String>(MaterialPageRoute(
                builder: (_) => CountriesPage(
                      current: _current,
                    )));
            if (x != null) {
              setState(() => _current = x);
              _bloc.add(_current);
            }
          }),
          pinned: true,
          // floating: true,
          // snap: true,
          expandedHeight: height * 0.175,
          flexibleSpace: SmartRefresher(
            controller: _controller,
            enablePullDown: true,
            onRefresh: () => _bloc.add(_current),
            child: FlexibleSpaceBar(
              title: Text('COVID-19'),
              background: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Image.asset('assets/stay-home-logo.png',
                        fit: BoxFit.contain),
                  ],
                ),
              ),
            ),
          ),
        );

    _body() => Stack(
          fit: StackFit.expand,
          children: [
            Background(),
            Mask(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: ChartOne(),
                  ),
                ),
                Developer(name: 'JOHN'),
              ],
            ),
          ],
        );

    return MultiBlocProvider(
      providers: [
        BlocProvider<StatsBloc>(create: (context) => _bloc),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener<StatsBloc, StatsState>(
            listener: (context, state) {
              if (state.hasError)
                debugPrint(state.message);
              else if (!state.isLoading) {
                state.stats?.forEach((e) => debugPrint(e.toString()));
                _controller.refreshCompleted();
              }
            },
          ),
        ],
        child: WillPopScope(
          onWillPop: () async => false,
          child: SafeArea(
            child: Scaffold(
              body: CustomScrollView(
                slivers: [
                  _appBar(),
                  SliverFillRemaining(child: _body()),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
