import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../commons/index.dart';
import '../../models/index.dart';
import '../index.dart';

class ChartOne extends StatefulWidget {
  @override
  _ChartOneState createState() => _ChartOneState();
}

class _ChartOneState extends State<ChartOne> {
  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var width = MediaQuery.of(context).size.width * 0.6;

    _stats(StatsModel model) => SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RichText(
                    text: TextSpan(
                      text: model.country,
                      style: theme.textTheme.headline4
                          .copyWith(fontWeight: FontWeight.w600),
                    ),
                  ),
                  RichText(
                    text: TextSpan(
                      text: NumberFormat.decimalPattern('es')
                          .format(model.cases.total),
                      style: theme.textTheme.headline4
                          .copyWith(fontWeight: FontWeight.w300),
                      children: [
                        TextSpan(
                          text: ' casos',
                          style: theme.textTheme.subtitle1,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(
                      text: DateFormat.yMMMEd('es').format(model.time),
                      style: theme.textTheme.headline6,
                    ),
                  ),
                  Column(
                    children: [
                      RichText(
                        text: TextSpan(
                          text: '—',
                          style: theme.textTheme.headline6.copyWith(
                              color: Colors.orange,
                              fontWeight: FontWeight.bold),
                          children: [
                            TextSpan(
                              text: ' ${model.cases.newCases ?? "+0"}',
                              style: theme.textTheme.headline6
                                  .copyWith(fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                      ),
                      RichText(
                        text: TextSpan(
                          text: '—',
                          style: theme.textTheme.headline6.copyWith(
                              color: Colors.red, fontWeight: FontWeight.bold),
                          children: [
                            TextSpan(
                              text: ' ${model.deaths.newCases ?? "+0"}',
                              style: theme.textTheme.headline6
                                  .copyWith(fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
              Row(
                children: [
                  PieChart(
                    PieChartData(
                      sections: model.casesSectionData(theme),
                      borderData: FlBorderData(show: false),
                      sectionsSpace: 0.0,
                      centerSpaceRadius: 48.0,
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        LegendItem(
                          text: model.totalActive,
                          description: 'Activos',
                          color: Colors.orange,
                        ),
                        LegendItem(
                          text: model.totalRecovered,
                          description: 'Recuperados',
                          color: Colors.blue,
                        ),
                        LegendItem(
                          text: model.totalDeaths,
                          description: 'Muertos',
                          color: Colors.red,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _InlineItem(
                    width: width,
                    fraction: model.criticalFraction,
                    text: model.totalCritical,
                  ),
                  Text(
                    ' — Críticos',
                    style: theme.textTheme.bodyText2
                        .copyWith(fontWeight: FontWeight.w300),
                  ),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _InlineItem(
                    width: width,
                    fraction: model.testsFraction,
                    text: model.totalTests,
                    foreground: Coolors.metallicSeaweed,
                  ),
                  Text(
                    ' — Pruebas positivas',
                    style: theme.textTheme.bodyText2
                        .copyWith(fontWeight: FontWeight.w300),
                  ),
                ],
              ),
            ],
          ),
        );

    return BlocBuilder<StatsBloc, StatsState>(
      builder: (context, state) => state.isLoading
          ? CovidProgressAnimated()
          : state.stats != null && state.stats.isNotEmpty
              ? _stats(state.stats[0])
              : Center(),
    );
  }
}

class _InlineItem extends StatelessWidget {
  const _InlineItem({
    Key key,
    @required this.width,
    @required this.fraction,
    @required this.text,
    this.background = Coolors.lightGray,
    this.foreground = Colors.red,
  }) : super(key: key);

  final double width, fraction;
  final String text;
  final Color background, foreground;

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var progress = width;
    if (fraction <= 1) progress = width * fraction;

    return Padding(
      padding: const EdgeInsets.only(bottom: 12.0),
      child: Row(
        children: [
          Stack(
            children: [
              Container(
                width: width,
                height: 24.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4.0),
                  color: background,
                ),
              ),
              Container(
                width: progress,
                height: 24.0,
                decoration: BoxDecoration(
                  borderRadius:
                      BorderRadius.horizontal(left: Radius.circular(4.0)),
                  color: foreground,
                ),
              ),
              Container(
                width: width,
                height: 24.0,
                padding: EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      text,
                      style: theme.textTheme.bodyText2
                          .copyWith(fontWeight: FontWeight.w300),
                    ),
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class LegendItem extends StatelessWidget {
  const LegendItem({
    Key key,
    @required this.text,
    @required this.description,
    @required this.color,
  }) : super(key: key);

  final String text, description;

  final Color color;

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return Container(
      padding: const EdgeInsets.symmetric(vertical: 4.0),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: 16.0,
            height: 32.0,
            decoration: BoxDecoration(
              // shape: BoxShape.circle,
              color: color,
              borderRadius: BorderRadius.circular(4.0),
            ),
          ),
          SizedBox(width: 8.0),
          Column(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                text,
                style: theme.textTheme.bodyText1
                    .copyWith(fontWeight: FontWeight.w300),
              ),
              Text(
                description,
                style: theme.textTheme.bodyText2
                    .copyWith(fontWeight: FontWeight.w300),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
