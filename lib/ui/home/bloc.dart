import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../commons/index.dart';
import '../../models/index.dart';

@immutable
class StatsState extends Equatable {
  final bool isLoading, hasError;
  final String message;
  final List<StatsModel> stats;

  StatsState(
      {this.isLoading = false,
      this.hasError = false,
      this.message,
      this.stats});

  factory StatsState.init() => StatsState();

  factory StatsState.loading() => StatsState(isLoading: true);

  factory StatsState.error(Exception _) =>
      StatsState(hasError: true, message: _.toString());

  factory StatsState.success(List<StatsModel> stats) =>
      StatsState(stats: stats);

  @override
  List<Object> get props => [isLoading, hasError, message];
}

class StatsBloc extends Bloc<String, StatsState> {
  @override
  StatsState get initialState => StatsState.init();

  @override
  Stream<StatsState> mapEventToState(
    String event,
  ) async* {
    yield StatsState.loading();
    try {
      // await Future.delayed(Duration(milliseconds: 2000));
      var response = await RapidAPI.statistics(event);
      yield StatsState.success(response);
    } catch (_) {
      yield StatsState.error(_);
    }
  }
}
