import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import '../models/index.dart';

class RapidAPI {
  static const String url = 'https://covid-193.p.rapidapi.com/';
  static const Map<String, String> header = {
    'x-rapidapi-host': 'covid-193.p.rapidapi.com',
    'x-rapidapi-key': '878d8a194bmsh56b262c805f12d9p130a6djsn23831af2799c',
  };

  static Future<BaseResponseModel> get(
      String route /*, Map<String, String> payload*/) async {
    BaseResponseModel _response;
    var request = http.Request('get', Uri.parse('$url$route'));
    request.headers.addEntries(header.entries);
    // request.bodyFields = payload;
    var response = await request.send();
    if (response.statusCode == 200) {
      var body = await response.stream.toBytes();
      var _payload = json.decode(utf8.decode(body));
      _response = BaseResponseModel(_payload);
    } else
      debugPrint(response.reasonPhrase);
    return _response;
  }

  static Future<List<CountryModel>> countries([String text]) async {
    var response =
        await get(text.isNotEmpty ? 'countries?search=$text' : 'countries');
    if (response != null) {
      debugPrint('countries: ${response.results}');
      return response.response?.map((e) => CountryModel(e))?.toList();
    }
    return null;
  }

  static Future<List<StatsModel>> statistics(
      [String country = 'Ecuador']) async {
    var response = await get('statistics?country=$country');
    if (response != null) {
      debugPrint('statistics: ${response.results}');
      return response.response?.map((e) => StatsModel(e))?.toList();
    }
    return null;
  }
}
