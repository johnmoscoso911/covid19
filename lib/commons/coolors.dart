import 'package:flutter/material.dart';

class Coolors {
  static const MaterialColor metallicSeaweed =
      const MaterialColor(0xFF2d848a, <int, Color>{
    50: Color(0xFF93d7dc),
    100: Color(0xFF84d1d7),
    200: Color(0xFF65c6cd),
    300: Color(0xFF46bbc3),
    400: Color(0xFF37a2a9),
    500: Color(0xFF2d848a),
    600: Color(0xFF23676c),
    700: Color(0xFF194a4d),
    800: Color(0xFF0f2c2e),
    900: Color(0xFF050f0f),
  });
  static const MaterialColor lightGray =
      const MaterialColor(0xFFc3d2d5, <int, Color>{
    50: Color(0xFFffffff),
    100: Color(0xFFf3f6f7),
    200: Color(0xFFe7edee),
    300: Color(0xFFdbe4e6),
    400: Color(0xFFcfdbde),
    500: Color(0xFFc3d2d5),
    600: Color(0xFFabc0c4),
    700: Color(0xFF93aeb4),
    800: Color(0xFF7b9ca3),
    900: Color(0xFF648990),
  });
  static const MaterialColor smokyBlack =
      const MaterialColor(0xFF0f0e0e, <int, Color>{
    50: Color(0xFF746d6d),
    100: Color(0xFF696363),
    200: Color(0xFF544f4f),
    300: Color(0xFF3f3b3b),
    400: Color(0xFF2a2828),
    500: Color(0xFF0f0e0e),
    600: Color(0xFF0b0a0a),
    700: Color(0xFF0b0a0a),
    800: Color(0xFF0b0a0a),
    900: Color(0xFF0b0a0a),
  });
}
// static const MaterialColor metallicSeaweed =
//       const MaterialColor(0xFF, <int, Color>{
//     50: Color(0xFF),
//     100: Color(0xFF),
//     200: Color(0xFF),
//     300: Color(0xFF),
//     400: Color(0xFF),
//     500: Color(0xFF),
//     600: Color(0xFF),
//     700: Color(0xFF),
//     800: Color(0xFF),
//     900: Color(0xFF),
//   });
